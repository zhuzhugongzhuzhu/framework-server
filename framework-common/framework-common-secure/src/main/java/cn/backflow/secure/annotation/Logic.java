package cn.backflow.secure.annotation;


/**
 * Permission logic.
 *
 * @author backflow
 */
public enum Logic {
    AND, OR
}