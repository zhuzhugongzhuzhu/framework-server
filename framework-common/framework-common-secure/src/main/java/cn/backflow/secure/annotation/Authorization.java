package cn.backflow.secure.annotation;

import java.lang.annotation.*;

/**
 * Mark a resource requires authorization.
 *
 * @author backflow
 */
@Inherited
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorization {

    /**
     * 单个权限
     */
    String value() default "";

    /**
     * 多个权限
     */
    String[] permissions() default {};

    /**
     * 多个权限时, 权限之间的关系
     */
    Logic logic() default Logic.AND;

}
