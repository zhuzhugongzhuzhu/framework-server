package cn.backflow.data.entity;

import java.util.Date;

public abstract class CommonEntity extends BaseEntity {

    protected Date created;

    protected Date updated;

    public Date getCreated() {
        return created;
    }

    public CommonEntity setCreated(Date created) {
        this.created = created;
        return this;
    }

    public Date getUpdated() {
        return updated;
    }

    public CommonEntity setUpdated(Date updated) {
        this.updated = updated;
        return this;
    }

    /*
    public CommonEntity setCreatedString(String createdString) {
        return setCreated(Dates.parseDate(createdString));
    }

    public String getCreatedString() {
        return Dates.formatDate(getCreated());
    }

    public CommonEntity setUpdatedString(String updatedString) {
        return setUpdated(Dates.parseDate(updatedString));
    }

    public String getUpdatedString() {
        return Dates.formatDate(getUpdated());
    }
    */
}