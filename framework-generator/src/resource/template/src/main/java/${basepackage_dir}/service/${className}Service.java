package ${basepackage}.service;

<#assign className=table.className>
<#assign classNameLower=className?uncap_first>
<#include"/java_imports.include">
import org.springframework.stereotype.Service;

@Service
public class ${className}Service extends BaseService<${className},${table.idColumn.javaType}>{

	@Autowired
	private ${className}Repository ${classNameLower}Repository;

<#list table.columns as column>
	<#if column.unique && !column.pk>
	@Transactional(readOnly=true)
	public ${className} getBy${column.columnName}(${column.javaType} v) {
		return ${classNameLower}Repository.getBy${column.columnName}(v);
	}	
	</#if>
</#list>
}