package cn.backflow.scheduling;


import cn.backflow.scheduling.service.JobService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest
@RunWith(SpringRunner.class)
@Import(TestScheduleCustomizer.class)
public class ScheduleApplicationTest {

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private JobService jobService;

    /**
     * 添加打印当前时间的任务
     */
    @Test
    public void addPrintCurrentTimeJob() throws SchedulerException {
        jobService.jobs().forEach(System.out::println);
    }

    /**
     * 删除打印当前时间的任务
     */
    @Test
    public void deletePrintCurrentTimeJob() {

    }
}
